const moment = require('moment');
const util = require('../lib/util');

function getScheduleStartAt(schedule, first=false) {
  const now = moment();

  let day = null;
  if(schedule.DoW !== '*' && (schedule.DoW - 1) !== now.day()) {
    const dow = schedule.DoW - 1;
    if(first && dow < now.day()) {
      while(dow !== now.day())
        now.add(1, 'days');
    }else now.add(dow - now.day(), 'days');
    day = now.format('DD');
  }else day = schedule.Day === '*'? now.format('DD'): util.addZero(schedule.Day);

  const year = schedule.Year === '*'? now.format('YYYY'): schedule.Year;
  const month = schedule.Month === '*'? now.format('MM'): util.addZero(schedule.Month);
  const hour = schedule.Hour === '*'? now.format('HH'): util.addZero(schedule.Hour);
  const minute = schedule.Minute === '*'? now.format('mm'): util.addZero(schedule.Minute);
  const second = schedule.Second === '*'? now.format('ss'): util.addZero(schedule.Second);

  return moment(`${year}-${month}-${day} ${hour}:${minute}:${second}`);
}


function getEventStartAt(config) {
  const now = moment();

  for(let i = 0; i < config.length; i++) {
    const startAt = getScheduleStartAt(config[i]);
    if(startAt.diff(now, 'seconds') > 0)
      return startAt.format('YYYY-MM-DD HH:mm:ss');
  }

  const schedule = config[0];
  while(true) {
    const startAt = getScheduleStartAt(schedule, true);

    if(schedule.Day === '*') startAt.add(1, 'days');
    else if(schedule.Month === '*') startAt.add(1, 'months');
    else if(schedule.Year === '*') startAt.add(1, 'years');
    else return null;

    if(startAt.diff(now, 'seconds') > 0)
      return startAt.format('YYYY-MM-DD HH:mm:ss');
  }
}

function secondsToTime(totalSeconds) {
  const hours = Math.floor(totalSeconds / 3600);
  totalSeconds %= 3600;
  const minutes = Math.floor(totalSeconds / 60);
  const seconds = totalSeconds % 60;
  if(hours > 24) {
    const days = Math.round(hours / 24);
    return `${days} ${days === 1? 'day': 'days'}`;
  }else return `${util.addZero(hours)}:${util.addZero(minutes)}:${util.addZero(seconds)}`;
}

function getEvents(events) {
  return events.map(event => {
    const startAt = getEventStartAt(event.config);
    const seconds = moment(startAt).diff(moment(), 'seconds');
    const timedown = secondsToTime(seconds);
    return {
      name: event.name,
      seconds,
      timedown,
      startAt
    };
  });
}

module.exports = (events=[], callback) => {
  callback(getEvents(events));
  setInterval(() => callback(getEvents(events)), 1000);
}
