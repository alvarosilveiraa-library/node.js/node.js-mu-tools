const execFile = require('child_process').execFile;

module.exports = {
  addZero(n) {
    if(n < 10) return '0' + n;
    else return n;
  },
  dec2Hex(number) {
    if(number < 0)
      number = 0xFFFFFFFF + number + 1;
    return number.toString(16).toUpperCase();
  },
  bytes2Value(type, bytes) {
    switch(type) {
      case 'CHAR':
        return Buffer.from(bytes.map(char => char || 32)).toString('utf8');
      case 'BYTE':
        return bytes[0];
      case 'WORD':
        return ((bytes[1] & 0xFF) << 8) | (bytes[0] & 0xFF);
      case 'DWORD':
        return (bytes[0]) | (bytes[1] << 8) | (bytes[2] << 16) | (bytes[3] << 24);
      case 'FLOAT':
        const value = bytes[0] ^ bytes[1] << 8 ^ bytes[2] << 16 ^ bytes[3] << 24;
        const int8 = new Int8Array(4);
        const int32 = new Int32Array(int8.buffer, 0, 1);
        const float32 = new Float32Array(int8.buffer, 0, 1);
        int32[0] = value;
        return float32[0].toFixed(6);
    }
  },
  value2Bytes(type, value) {
    switch(type) {
      case 'CHAR':
        const bytes = value.trim().split('').map(char => char.charCodeAt(0));
        if(bytes.length < value.length) {
          for(let i = bytes.length - 1; i < value.length; i++)
            bytes.push(0);
        }
        return bytes;
      case 'BYTE':
        return [Number(value)];
      case 'WORD':
        return [Number(value) & 0xFF, (Number(value) >> 8) & 0xFF];
      case 'DWORD':
        return [Number(value) & 0xFF, (Number(value) >> 8) & 0xFF, (Number(value) >> 16) & 0xFF, (Number(value) >> 24) & 0xFF];
      case 'FLOAT':
        const float32 = new Float32Array(1);
        float32[0] = Number(value);
        const int8 = new Int8Array(float32.buffer);
        return int8;
    }
  },
  execFileAsync(filepath, args) {
    return new Promise((resolve, reject) => {
      execFile(filepath, args, (err, stdout) => {
        if(err) reject(err);
        else {
          const response = stdout.replace(/\r/g, '').split(/\n/);
          resolve(response);
        }
      });
    });
  }
}
