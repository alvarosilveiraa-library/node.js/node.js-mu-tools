const bmd = require('./src/bmd');
const registry = require('./src/registry');
const system = require('./src/system');
const events = require('./src/events');

exports.bmd = bmd;
exports.registry = registry;
exports.system = system;
exports.events = events;

module.exports = {
	bmd,
  registry,
  system,
  events
}
