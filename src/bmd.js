const fs = require('fs');
const util = require('../lib/util');
const config = require('../config/bmd.json');

function calcCRC32(buffer, encKey) {
    // let decKey = encKey << 9;
    // for(let i = 0; i < buffer.length - 4; i += 4) {
    //     const hex = buffer2Hex(buffer, i);
    //     const rest = (encKey + (i >> 2)) % 2;
    //     if(rest && rest === 1)
    //         decKey += Number(hex);
    //     else decKey ^= Number(hex);

    //     if(!(i % 10)) decKey ^= (decKey + encKey) >> ((i >> 2) % 8 + 1);
    // }
    // return decKey;
    return [0x4e, 0x9a, 0x6c, 0x71];
}

function encDecXOR(xorEnc, xorKeys) {
    const xorDec = [];
    for(let i = 0; i < xorEnc.length; i++) {
        if(Array.isArray(xorKeys))
            xorDec[i] = xorEnc[i] ^ xorKeys[i % xorKeys.length];
        else if(typeof xorKeys === 'object') {
            const index = parseInt(i / xorKeys.interval);
            const keys = xorKeys.data[index % xorKeys.data.length];
            xorDec[i] = xorEnc[i] ^ keys[i % keys.length];
        }
    }
    return xorDec;
}

module.exports = {
  config,
  decrypt({
    size=0,
    xor=[],
    model=[],
    input='',
    output=''
  }) {
    return new Promise((resolve, reject) => {
      let file = '';
      const buffer = fs.readFileSync(input);
      // console.log(buffer.toString('hex'));
      const decrypted = encDecXOR(buffer, xor);
      for(let i = 0; i < (buffer.length - 4) / size; i++) {
        const bufferLine = decrypted.slice(i * size, i * size + size);
        let line = '';
        model.forEach(item => {
          const type = item.split('=')[0];
          const bytes = Number(item.split('=')[1]);
          const value = bufferLine.splice(0, bytes);
          line += util.bytes2Value(type, value) + '\t';
        });
        file += line + '\n';
      }
      fs.writeFile(output, file, err => {
        if(err) reject(err);
        else resolve();
      });
    });
  },
  encrypt({
    xor=[],
    model=[],
    input='',
    output=''
  }) {
    return new Promise((resolve, reject) => {
      const buffer = [];
      const text = fs.readFileSync(input, 'utf8');
      text.split('\n').slice(0, -1).forEach(line => {
        const bufferLine = line.split('\t');
        model.forEach((item, i) => {
          const type = item.split('=')[0];
          const value = bufferLine[i];
          buffer.push(...util.value2Bytes(type, value));
        });
      });
      const encrypted = encDecXOR(buffer, xor);
      const crc32 = calcCRC32(encrypted, 0x2BC1);
      encrypted.push(...crc32);
      fs.writeFile(output, Buffer.from(encrypted), err => {
        if(err) reject(err);
        else resolve();
      });
    });
  }
}

// const args = process.argv.slice(2);
// const method = args[0];
// const name = args[1];

// if(method && name && config[name]) {
//     switch(method) {
//         case 'decrypt':
//             readBmd(...config[name]);
//             break;
//         case 'encrypt':
//             writeBmd(...config[name].slice(1));
//             break;
//     }
// }
