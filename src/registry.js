const path = require('path');
const util = require('../lib/util');
const filepath = path.join(__dirname, '../lib/registry.exe');

module.exports = {
  async readAll() {
    const args = await util.execFileAsync(filepath, ['--read', 'all']);
    return {
      username: args[0],
      resolution: Number(args[1]),
      music: Number(args[2]),
      sound: Number(args[3]),
      windowMode: Number(args[4])
    }
  },
  writeAll(
    username='',
    resolution=0,
    music=false,
    sound=true,
    windowMode=true
  ) {
    return util.execFileAsync(filepath, [
      '--username',
      username,
      '--resolution',
      resolution,
      '--music',
      music? 1: 0,
      '--sound',
      sound? 1: 0,
      '--window',
      windowMode? 1: 0
    ]);
  },
  writeUsername(username='') {
    return util.execFileAsync(filepath, ['--username', username]);
  },
  writeResolution(resolution=0) {
    return util.execFileAsync(filepath, ['--resolution', resolution]);
  },
  writeMusic(music=false) {
    return util.execFileAsync(filepath, ['--music', music? 1: 0]);
  },
  writeSound(sound=true) {
    return util.execFileAsync(filepath, ['--sound', sound? 1: 0]);
  },
  writeWindowMode(windowMode=true) {
    return util.execFileAsync(filepath, ['--window', windowMode? 1: 0]);
  }
}
