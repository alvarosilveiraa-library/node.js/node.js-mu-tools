# Mu Tools

### BMD
> encrypt and decrypt .bmd files
```
  const bmd = require('mu-tools').bmd;
  bmd.decrypt({
    ...bmd.config.masterSkillTreeData,
    input: 'C:\\path\\to\\client\\masterskilltreedata.bmd',
    output: ''
  }).then(() => console.log('RESOLVED'))
  .catch(err => console.log(err));
```

### Webzen Registry
> Read/Write windows registry
```
  const registry = require('mu-tools').registry;
  registry.readAll()
    .then(data => console.log(data))
    .catch(err => console.log(err));
```
