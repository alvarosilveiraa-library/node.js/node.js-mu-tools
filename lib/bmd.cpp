#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <windows.h>

// const int ITEM_LINE_SIZE = 84;
// const int ITEM_TOTAL_LINE_SIZE = ITEM_LINE_SIZE << 13;
const int ITEM_LINE_SIZE = 24;
const int ITEM_TOTAL_LINE_SIZE = ITEM_LINE_SIZE << 9;

struct ItemEx {
  BYTE b1;
  WORD w1;
  BYTE b2;
  BYTE b3;
  BYTE b4;
  BYTE b5;
  DWORD d1;
  DWORD d2;
  DWORD d3;
  float f1;
};
ItemEx *pItem;

// struct ItemEx {
//   char ItemName[30];
//   BYTE b31;
//   BYTE b32;
//   BYTE b33;
//   BYTE b34;
//   BYTE b35;
//   BYTE b36;
//   BYTE b37;
//   BYTE b38;
//   BYTE b39;
//   BYTE b40;
//   BYTE b41;
//   BYTE b42;
//   BYTE b43;
//   BYTE b44;
//   BYTE b45;
//   BYTE b46;
//   BYTE b47;
//   BYTE b48;
//   BYTE b49;
//   BYTE b50;
//   BYTE b51;
//   BYTE b52;
//   BYTE b53;
//   BYTE b54;
//   BYTE b55;
//   BYTE b56;
//   BYTE b57;
//   BYTE b58;
//   BYTE b59;
//   BYTE b60;
//   BYTE b61;
//   BYTE b62;
//   BYTE b63;
//   BYTE b64;
//   BYTE b65;
//   BYTE b66;
//   BYTE b67;
//   BYTE b68;
//   BYTE b69;
//   BYTE b70;
//   BYTE b71;
//   BYTE b72;
//   BYTE b73;
//   BYTE b74;
//   BYTE b75;
//   BYTE b76;
//   BYTE b77;
//   BYTE b78;
//   BYTE b79;
//   BYTE b80;
//   BYTE b81;
//   BYTE b82;
//   BYTE b83;
//   BYTE b84;
// }; 
// ItemEx *pItem;

size_t GetFileSize(void *pSrcBuf, size_t Size, size_t Count, FILE *fp)
{ 
  fseek(fp, 0 , SEEK_END);
  int ItemSz = ftell(fp);
  rewind(fp);
  size_t Result = fread(pSrcBuf, Count, Size, fp);
  return Result;
}

int DecryptKey(int pSrcBuf, int Size, unsigned short EncKey)
{
  int Dst;
  int DecryptedKey = EncKey << 9;

  for(unsigned int i = 0; i <= Size - 4; i += 4) {
    memcpy(&Dst, (const void*)(i + pSrcBuf), 4U);
    int rest = (EncKey + (i >> 2)) % 2;
    if(rest) {
      if(rest == 1)
        DecryptedKey += Dst;
    }else {
      DecryptedKey ^= Dst;
    }

    if(!(i % 10)) 
      DecryptedKey ^= (unsigned int)(DecryptedKey + EncKey) >> ((i >> 2) % 8 + 1);
  }
  return DecryptedKey;
}

char *DecryptXorEnc(char *lpSrcBuf, int Size)
{
  BYTE XorKeys[3] = { 0xFC, 0xCF, 0xAB };
  for (int i = 0; i < Size; ++i)
	  lpSrcBuf[i] ^= XorKeys[i % 3];
  return lpSrcBuf;
}

bool ReadItemBmd(char *pFileName)
{
  pItem = new ItemEx();

  char *lpDstBuf = new char[ITEM_TOTAL_LINE_SIZE];
  char *lpSrcBuf = new char[ITEM_TOTAL_LINE_SIZE];
	
  FILE *fp = NULL;
  if((fp = fopen(pFileName, "rb")) == NULL)
	  return false;

  FILE *sp = NULL;
  if((sp = fopen("C:\\Users\\alvar\\Downloads\\ItemDec.txt", "w")) == NULL)
	  return false;

  fread(lpDstBuf, 1, ITEM_TOTAL_LINE_SIZE, fp);

  for(int i = 0; i < ITEM_TOTAL_LINE_SIZE / ITEM_LINE_SIZE; ++i) {
    lpSrcBuf = DecryptXorEnc(&lpDstBuf[i * ITEM_LINE_SIZE], ITEM_LINE_SIZE);

    memcpy(pItem, &lpSrcBuf[0], ITEM_LINE_SIZE);

    fprintf(sp, "%d  %d  %d  %d  %d  %d  %d  %d  %d  %f\n", pItem->b1, pItem->w1, pItem->b2, pItem->b3, pItem->b4, pItem->b5, pItem->d1, pItem->d2, pItem->d3, pItem->f1);

    // fprintf(sp, "[%30s][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d][%3d]\n", 
    //   pItem->ItemName, pItem->b31, pItem->b32, pItem->b33, pItem->b34, pItem->b35, pItem->b36, pItem->b37, pItem->b38, pItem->b39, pItem->b40, pItem->b41, pItem->b42, pItem->b43, pItem->b44, pItem->b45, pItem->b46, pItem->b47, pItem->b48, pItem->b49, pItem->b50,
    //   pItem->b51, pItem->b52, pItem->b53, pItem->b54, pItem->b55, pItem->b56, pItem->b57, pItem->b58, pItem->b59, pItem->b60, pItem->b61, pItem->b62, pItem->b63, pItem->b64, pItem->b65, pItem->b66, pItem->b67, pItem->b68, pItem->b69, pItem->b70, pItem->b71, pItem->b72, 
    //   pItem->b73, pItem->b74, pItem->b75, pItem->b76, pItem->b77, pItem->b78, pItem->b79, pItem->b80, pItem->b81, pItem->b82, pItem->b83, pItem->b84);
    // }
  }

  delete [] lpDstBuf, lpSrcBuf;
  lpDstBuf = lpSrcBuf = NULL;
  delete pItem;
  pItem = NULL;

  fclose(fp);
  fclose(sp);
  return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (!ReadItemBmd("G:\\development\\projects\\mu-insonmia\\Cliente\\Data\\Local\\Eng\\masterskilltreedata_eng.bmd"))
	{
		MessageBoxA(NULL, "item.bmd - File corrupted.", "Error", MB_ICONERROR);
		::ExitProcess(0);
	}
	std::cout << "Press key!" << std::endl;
	std::cin.get();
	return 0;
}

	// byte incoming[4]={0XED,0X95,0XEB,0XBB};
	// float velocity = *((float*) incoming);