﻿using System;

namespace WebzenReg
{
    class Program
    {
        static void Main(string[] args)
        {
            Control control = new Control();
            for(int i = 0; i < args.Length; i += 2)
            {
                string arg = args[i + 1];
                switch(args[i])
                {
                    case "--read":
                        if(arg == "all")
                        {
                            Console.WriteLine(control.getID());
                            Console.WriteLine(control.getResolution());
                            Console.WriteLine(control.getMusicOnOff());
                            Console.WriteLine(control.getSoundOnOff());
                            Console.WriteLine(control.getWindowMode());
                        }
                        break;
                    case "--username":
                        control.setID(arg);
                        break;
                    case "--resolution":
                        int resolution = Int32.Parse(arg);
                        if(resolution >= 0 && resolution <= 8)
                            control.setResolution(resolution);
                        break;
                    case "--music":
                        int music = Int32.Parse(arg);
                        if(music == 0 || music == 1)
                            control.setMusicOnOff(music);
                        break;
                    case "--sound":
                        int sound = Int32.Parse(arg);
                        if(sound == 0 || sound == 1)
                            control.setSoundOnOff(sound);
                        break;
                    case "--window":
                        int window = Int32.Parse(arg);
                        if(window == 0 || window == 1)
                            control.setWindowMode(window);
                        break;
                }
            }
        }
    }
}
