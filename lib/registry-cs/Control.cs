﻿using System;
using Microsoft.Win32;

public class Control
{
    private string reg = "SOFTWARE\\Webzen\\Mu\\Config";

    public Control()
    {
        if (Registry.CurrentUser.OpenSubKey(@reg) == null)
            Registry.CurrentUser.CreateSubKey(@reg);

        RegistryKey get = Registry.CurrentUser.OpenSubKey(@reg);
        RegistryKey set = Registry.CurrentUser.CreateSubKey(@reg);

        if (get.GetValue("ID") == null)
            set.SetValue("ID", "", RegistryValueKind.String);

        if (get.GetValue("Resolution") == null)
            set.SetValue("Resolution", 1, RegistryValueKind.DWord);

        if (get.GetValue("MusicOnOff") == null)
            set.SetValue("MusicOnOff", 1, RegistryValueKind.DWord);

        if (get.GetValue("SoundOnOff") == null)
            set.SetValue("SoundOnOff", 1, RegistryValueKind.DWord);

        if (get.GetValue("VolumeLevel") == null)
            set.SetValue("VolumeLevel", 5, RegistryValueKind.DWord);

        if (get.GetValue("WindowMode") == null)
            set.SetValue("WindowMode", 1, RegistryValueKind.DWord);

        get.Close();
        set.Close();
    }

    public string getVersion()
    {
        string version;
        RegistryKey get = Registry.CurrentUser.OpenSubKey(@reg);
        version = get.GetValue("Version").ToString();
        get.Close();
        return version;

    }

    public string getID()
    {
        string username;
        RegistryKey get = Registry.CurrentUser.OpenSubKey(@reg);
        username = get.GetValue("ID").ToString();
        get.Close();
        return username;
    }

    public int getResolution()
    {
        int resolution;
        RegistryKey get = Registry.CurrentUser.OpenSubKey(@reg);
        resolution = (int)Convert.ToInt32(get.GetValue("Resolution"));
        get.Close();
        return resolution;
    }

    public int getMusicOnOff()
    {
        int musicOnOff;
        RegistryKey get = Registry.CurrentUser.OpenSubKey(@reg);
        musicOnOff = (int)Convert.ToInt32(get.GetValue("MusicOnOff"));
        get.Close();
        return musicOnOff;
    }

    public int getSoundOnOff()
    {
        int soundOnOff;
        RegistryKey get = Registry.CurrentUser.OpenSubKey(@reg);
        soundOnOff = (int)Convert.ToInt32(get.GetValue("SoundOnOff"));
        get.Close();
        return soundOnOff;
    }

    public int getVolumeLevel()
    {
        int volumeLevel;
        RegistryKey get = Registry.CurrentUser.OpenSubKey(@reg);
        volumeLevel = (int)Convert.ToInt32(get.GetValue("VolumeLevel"));
        get.Close();
        return volumeLevel;
    }

    public int getWindowMode()
    {
        int windowMode;
        RegistryKey get = Registry.CurrentUser.OpenSubKey(@reg);
        windowMode = (int)Convert.ToInt32(get.GetValue("WindowMode"));
        get.Close();
        return windowMode;
    }

    public void setVersion(string version)
    {
        RegistryKey set = Registry.CurrentUser.CreateSubKey(@reg);
        set.SetValue("Version", version, RegistryValueKind.String);
        set.Close();
    }

    public void setID(string username)
    {
        RegistryKey set = Registry.CurrentUser.CreateSubKey(@reg);
        set.SetValue("ID", username, RegistryValueKind.String);
        set.Close();
    }

    public void setResolution(int resolution)
    {
        RegistryKey set = Registry.CurrentUser.CreateSubKey(@reg);
        set.SetValue("Resolution", resolution, RegistryValueKind.DWord);
        set.Close();
    }

    public void setMusicOnOff(int musicOnOff)
    {
        RegistryKey set = Registry.CurrentUser.CreateSubKey(@reg);
        set.SetValue("MusicOnOff", musicOnOff, RegistryValueKind.DWord);
        set.Close();
    }

    public void setSoundOnOff(int soundOnOff)
    {
        RegistryKey set = Registry.CurrentUser.CreateSubKey(@reg);
        set.SetValue("SoundOnOff", soundOnOff, RegistryValueKind.DWord);
        set.Close();
    }

    public void setVolumeLevel(int volumeLevel)
    {
        RegistryKey set = Registry.CurrentUser.CreateSubKey(@reg);
        set.SetValue("VolumeLevel", volumeLevel, RegistryValueKind.DWord);
        set.Close();
    }

    public void setWindowMode(int windowMode)
    {
        RegistryKey set = Registry.CurrentUser.CreateSubKey(@reg);
        set.SetValue("WindowMode", windowMode, RegistryValueKind.DWord);
        set.Close();
    }
}