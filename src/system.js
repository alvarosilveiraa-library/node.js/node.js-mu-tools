const path = require('path');
const util = require('../lib/util');
const filepath = path.join(__dirname, '../lib/system.exe');

module.exports = main => util.execFileAsync(filepath, [main]);
