const fs = require('fs');

const path = 'C:\\Users\\alvar\\Downloads\\masterskilltree.txt';

const text = fs.readFileSync(path, 'utf8');

const fixLength = (value='', length=16) => {
    let text = value;
    if(value.length < length) {
        for(let i = 0; i < length - value.length; i++) {
            text += ' ';
        }
    }
    return text;
}

// const groupsJSON = text.split('end').map(group => {
// 	if(group) {
//         const groupEnterSplittedJson = [];
//         let groupJsonNames = null;
//         group.split(/\n/).forEach(groupEnterSplitted => {
//             if(/^\/\//.test(groupEnterSplitted))
//                 groupJsonNames = groupEnterSplitted.replace('//', '').split(/\s+/g);
//             else {
//                 const splittedGroup = groupEnterSplitted.split(/\s+/g);
//                 const parsedGroup = {};
//                 (groupJsonNames || splittedGroup.slice(0, -1).map((valut, i) => 'Value' + i))
//                     .forEach((name, i) => {
//                         parsedGroup[name] = splittedGroup[i];
//                     });
//                 groupEnterSplittedJson.push(parsedGroup);
//             }
//         });
//         return groupEnterSplittedJson;
//     }else return null;
// });

// console.log(groupsJSON);

const groupsText = text.split('end').map(group => {
	if(group) {
        return group.split(/\n/).map(groupEnterSplitted => {
            return groupEnterSplitted.split(/\s+/g).map(value => {
                return fixLength(value);
            }).join('')
        }).join('\n');
    }else return null;
}).join('end').replace(/\s+end/, '\nend');

fs.writeFileSync(path.replace('.', '_writted.'), groupsText);